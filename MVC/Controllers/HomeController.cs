﻿using Business;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        BiletService service = new BiletService();

        //60 saniyeliğine outputu cacheliyor
        [OutputCache(Duration = 60)]
        public ActionResult Index()
        {

            var result=service.KalkisVarisNoktalari();
            ViewBag.Noktalar =new SelectList(result.Data,"ID", "Ad");
            return View();
        }

        [HttpGet]
        
        public ActionResult SeferList(int kalkis,int varis,DateTime tarih,string firmaFilter=null,string seferTipiFilter=null,int siralama=1)
        {

            List<string> firmaNoList = new List<string>();
            List<string> seferTipList = new List<string>();

            if (!string.IsNullOrEmpty(firmaFilter))
            {
                firmaNoList = firmaFilter.Split(',').ToList();

            }
            if (!string.IsNullOrEmpty(seferTipiFilter))
            {
                seferTipList = seferTipiFilter.Split(',').ToList();

            }
            // Webservice den data çekiyor
            var result = service.SeferSorgula(kalkis,varis, tarih);

            if (result.Success)
            {
                // Filtrelemeler için SelectListler 
                var FirmaList = result.Data.GroupBy(x => new { x.FirmaNo, x.FirmaAdi })
                    .Select(x => new SelectListItem { Value = x.Key.FirmaNo, Text = x.Key.FirmaAdi,Selected= firmaNoList.Contains(x.Key.FirmaNo) })
                    .ToList();

                ViewBag.Firmalar = FirmaList;

                var SeferTipList = result.Data.GroupBy(x => new { x.OtobusKoltukYerlesimTipi })
                    .Select(x => new SelectListItem { Value = x.Key.OtobusKoltukYerlesimTipi,Text=x.Key.OtobusKoltukYerlesimTipi,Selected= seferTipList.Contains(x.Key.OtobusKoltukYerlesimTipi) })
                    .ToList();

                ViewBag.SeferTipleri = SeferTipList;

                var SiralamaList = new List<SelectListItem>()
                {
                    new SelectListItem{ Value="1",Text="Kalkış Saatine Göre" ,Selected=siralama==1},
                    new SelectListItem{Value="2",Text="Fiyata Göre",Selected=siralama==2}
                };

                ViewBag.Siralamalar = SiralamaList;

                // Servisden gelen Veri üzerinde Filtreleme ve sıralama
                result.Data=service.SeferFiltrele(result.Data, firmaNoList, seferTipList, siralama);

            }


            return View(result);
        }

        public ActionResult PnrSorgula(string pnr)
        {
            var result=service.PnrSorgula(pnr);
            return View(result);
        }
    }
}