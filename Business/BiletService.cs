﻿using DataAccess;
using DataAccess.ObiletServiceReference;
using Model.Results;
using Model.ViewModels;
using Model.WebServiceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Business
{
    public class BiletService
    {
        private readonly XElement yetki = new XElement("Kullanici", new XElement("Adi", "stajyerWS"), new XElement("Sifre", "2324423WSs099"));
        public IDataResult<List<KaraNokta>> KalkisVarisNoktalari()
        {
            ServiceSoapClient client = new ServiceSoapClient();


            XElement islem = new XElement("KaraNoktaGetirKomut");
            var serviceResult = client.XmlIslet(islem, yetki);

            var result = XmlSerialize.ConvertXmlToObject<KaraNoktalar>(serviceResult);
            return new SuccessDataResult<List<KaraNokta>>(result.KaraNokta);
        }

        public IDataResult<List<SeferListViewModel>> SeferSorgula(int kalkis, int varis, DateTime tarih)
        {
            ServiceSoapClient client = new ServiceSoapClient();


            XElement islem = new XElement("Sefer", new XElement("FirmaNo", "0"), new XElement("KalkisNoktaID", kalkis), new XElement("VarisNoktaID", varis)
                , new XElement("Tarih", tarih.ToString("yyyy-MM-dd")), new XElement("AraNoktaGelsin", "1"), new XElement("IslemTipi", "0"), new XElement("YolcuSayisi", "1")
                , new XElement("Ip", "127.0.0.1"));
            var serviceResult = client.XmlIslet(islem, yetki);

            if (serviceResult.Elements("Hata").Any())
            {
                return new ErrorDataResult<List<SeferListViewModel>>(serviceResult.Elements("Hata").First().Value);
            }

            var result = XmlSerialize.ConvertXmlToObject<NewDataSet>(serviceResult);

            var seferList = result.Table.Select(x => new SeferListViewModel
            {
                AcikParaKullanimAktifMi = x.AcikParaKullanimAktifMi,
                BiletFiyati1 = x.BiletFiyati1,
                BiletFiyatiInternet = x.BiletFiyatiInternet,
                BiletIptalAktifMi = x.BiletIptalAktifMi,
                DolulukKuraliVar = x.DolulukKuraliVar,
                DoluSeferMi = x.DoluSeferMi,
                FirmaAdi = x.FirmaAdi,
                FirmaNo = x.FirmaNo,
                FirmaSeferAciklamasi = x.FirmaSeferAciklamasi,
                GunBitimi = x.GunBitimi,
                Guzergah = x.Guzergah,
                HatNo = x.HatNo,
                HatSeferNo = x.HatSeferNo,
                ID = x.ID,
                IlkKalkisNokta = x.IlkKalkisNokta,
                IlkKalkisNoktaID = x.IlkKalkisNoktaID,
                IlkKalkisYeri = x.IlkKalkisYeri,
                KalkisNokta = x.KalkisNokta,
                KalkisNoktaID = x.KalkisNoktaID,
                KalkisTerminalAdi = x.KalkisTerminalAdi,
                KalkisTerminalAdiSaatleri = x.KalkisTerminalAdiSaatleri,
                KalkisYeri = x.KalkisYeri,
                KKZorunluMu = x.KKZorunluMu,
                MaximumRezerveTarihiSaati = x.MaximumRezerveTarihiSaati,
                MaxRzvZamani = x.MaxRzvZamani,
                NormalBiletFiyati = x.NormalBiletFiyati,
                OTipAciklamasi = x.OTipAciklamasi,
                OTipOzellik = x.OTipOzellik,
                OtobusKoltukYerlesimTipi = x.OtobusKoltukYerlesimTipi,
                OtobusPlaka = x.OtobusPlaka,
                OtobusTelefonu = x.OtobusTelefonu,
                OtobusTipi = x.OtobusTipi,
                O_Tip_Sinif = x.O_Tip_Sinif,
                Saat = x.Saat,
                SatisYonlendirilecekMi = x.SatisYonlendirilecekMi,
                SeferBosKoltukSayisi = x.SeferBosKoltukSayisi,
                SefereKadarIptalEdilebilmeSuresiDakika = x.SefereKadarIptalEdilebilmeSuresiDakika,
                SeferTakipNo = x.SeferTakipNo,
                SeferTipi = x.SeferTipi,
                SeferTipiAciklamasi = x.SeferTipiAciklamasi,
                StrSeyahatSuresi = StrSeyehatSuresi(x.SeyahatSuresi),
                SeyahatSuresi = x.SeyahatSuresi,
                SeyahatSuresiGosterimTipi = x.SeyahatSuresiGosterimTipi,
                Sinif_Farki = x.Sinif_Farki,
                SonVarisNokta = x.SonVarisNokta,
                SonVarisNoktaID = x.SonVarisNoktaID,
                SonVarisYeri = x.SonVarisYeri,
                Tarih = x.Tarih,
                Tesisler = x.Tesisler,
                ToplamSatisAdedi = x.ToplamSatisAdedi,
                Vakit = x.Vakit,
                VarisNokta = x.VarisNokta,
                VarisNoktaID = x.VarisNoktaID,
                VarisYeri = x.VarisYeri,
                YaklasikSeyahatSuresi = x.YaklasikSeyahatSuresi,
                YerelInternetSaat = x.YerelInternetSaat,
                YerelSaat = x.YerelSaat,
                Ozellikler = GetOTipOzellik(result.OTipOzellik, x.OTipOzellik)
            }).ToList();

            return new SuccessDataResult<List<SeferListViewModel>>(seferList);
        }
        public List<SeferListViewModel> SeferFiltrele(List<SeferListViewModel> seferList,List<string> firmaNoList, List<string> seferTipiList, int siralama = 1)
        {
            if (firmaNoList.Count > 0)
            {
                seferList = seferList.Where(x => firmaNoList.Contains(x.FirmaNo)).ToList();

            }
            if (seferTipiList.Count > 0)
            {
                seferList = seferList.Where(x => seferTipiList.Contains(x.OtobusKoltukYerlesimTipi)).ToList();

            }
            if (siralama == 1)
            {
                seferList = seferList.OrderBy(x => x.YerelInternetSaat).ToList();
            }
            else
            {
                seferList = seferList.OrderBy(x => x.BiletFiyatiInternet).ToList();
            }
            return seferList;
        }
        public IDataResult<BiletViewModel> PnrSorgula(string pnr)
        {
            using (var db = new OBiletDbContext())
            {
                var bilet = db.Bilet.Join(db.Sefer, x => x.SeferId, y => y.Id, (x, y) => new { Bilet = x, Sefer = y })
                    .Select(x => new BiletViewModel
                    {
                        Id=x.Bilet.Id,
                        Pnr=x.Bilet.Pnr,
                        Telefon=x.Bilet.Telefon,
                        ToplamUcret=x.Bilet.ToplamUcret,
                        EPosta=x.Bilet.EPosta,
                        SeferId=x.Bilet.SeferId,
                        Sefer=new SeferViewModel
                        {
                            FirmaAdi=x.Sefer.FirmaAdi,
                            Guzergah=x.Sefer.Guzergah,
                            Id=x.Sefer.Id,
                            KalkisNoktasi=x.Sefer.KalkisNoktasi,
                            KalkisTarihi=x.Sefer.KalkisTarihi,
                            KoltukTipi=x.Sefer.KoltukTipi,
                            Peron=x.Sefer.Peron,
                            SeferNo=x.Sefer.SeferNo,
                            SeferTipi=x.Sefer.SeferTipi,
                            Sure=x.Sefer.Sure,
                            VarisNoktasi=x.Sefer.VarisNoktasi
                        },
                        
                        YolcuList=x.Bilet.Yolcu.Select(z=>new YolcuViewModel { 
                            AdSoyad=z.AdSoyad,
                            BiletId=z.BiletId,
                            Cinsiyet=z.Cinsiyet,
                            Durum=z.Durum,
                            Id=z.Id,
                            KoltukNo=z.KoltukNo
                        }).ToList()
                    }).FirstOrDefault(x=>x.Pnr== pnr);

                if (bilet==null)
                {
                    return new ErrorDataResult<BiletViewModel>("Pnr koduna ait bilet bulunamadı.");
                }


                return new SuccessDataResult<BiletViewModel>(bilet);
            }
                
        }

        private List<TipOzellikViewModel> GetOTipOzellik(List<OTipOzellik> otipOzellikler, string strOzellik)
        {
            List<TipOzellikViewModel> list = new List<TipOzellikViewModel>();

            for (var i = 0; i < strOzellik.Length; i++)
            {
                if (strOzellik[i] == '1')
                {
                    var otipOzellik = otipOzellikler.First(x => x.O_Tip_Ozellik == i.ToString());
                    list.Add(new TipOzellikViewModel
                    {
                        O_Tip_Ozellik = otipOzellik.O_Tip_Ozellik,
                        O_Tip_Ozellik_Aciklama = otipOzellik.O_Tip_Ozellik_Aciklama,
                        O_Tip_Ozellik_Detay = otipOzellik.O_Tip_Ozellik_Detay,
                        O_Tip_Ozellik_Icon = otipOzellik.O_Tip_Ozellik_Icon
                    });
                }
            }


            return list;
        }

        private string StrSeyehatSuresi(DateTime tarih)
        {
            StringBuilder sb = new StringBuilder();
            var gun = tarih.Day - 1;
            var saat = tarih.Hour;
            var dakika = tarih.Minute;

            if (gun != 0)
            {
                sb.Append(gun + " Gün ");
            }
            if (saat != 0)
            {
                sb.Append(saat + " Saat ");
            }
            if (dakika != 0)
            {
                sb.Append(dakika + " dakika");
            }

            return sb.ToString();
        }

    }
}
