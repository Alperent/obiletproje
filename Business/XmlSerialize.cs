﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Business
{
    public class XmlSerialize
    {
        public static List<T> ConvertXmlToObjects<T>(XElement xml)
        {
            List<T> list;
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));
            list = (List<T>)serializer.Deserialize(xml.CreateReader());
            return list;
        }

        public static T ConvertXmlToObject<T>(XElement xml)
        {
            T obj;
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            obj = (T)serializer.Deserialize(xml.CreateReader());
            return obj;
        }
    }
}
