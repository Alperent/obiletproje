﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ViewModels
{
    public class BiletViewModel
    {
        public int Id { get; set; }
        public string Pnr { get; set; }

        public int SeferId { get; set; }

        public string EPosta { get; set; }

        public string Telefon { get; set; }

        public double ToplamUcret { get; set; }

        public SeferViewModel Sefer { get; set; }
        public List<YolcuViewModel> YolcuList { get; set; }
    }

    public class SeferViewModel
    {
        public int Id { get; set; }

        public string SeferNo { get; set; }

        public string FirmaAdi { get; set; }

        public string Guzergah { get; set; }

        public string KalkisNoktasi { get; set; }

        public string VarisNoktasi { get; set; }

        public DateTime KalkisTarihi { get; set; }

        public string Sure { get; set; }

        public string KoltukTipi { get; set; }

        public string SeferTipi { get; set; }

        public int Peron { get; set; }
    }
    public class YolcuViewModel
    {
        public int Id { get; set; }

        public int BiletId { get; set; }

        public string AdSoyad { get; set; }

        public string Durum { get; set; }

        public int KoltukNo { get; set; }
        public string Cinsiyet { get; set; }
    }
}
