﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.ViewModels
{
    public class SeferListViewModel
    {
		
		public int ID { get; set; }
		public string Vakit { get; set; }
		public string FirmaNo { get; set; }
		public string FirmaAdi { get; set; }
		public DateTime YerelSaat { get; set; }
		public DateTime YerelInternetSaat { get; set; }
		public DateTime Tarih { get; set; }
		public string GunBitimi { get; set; }
		public DateTime Saat { get; set; }
		public string HatNo { get; set; }
		public string IlkKalkisYeri { get; set; }
		public string SonVarisYeri { get; set; }
		public string KalkisYeri { get; set; }
		public string VarisYeri { get; set; }
		public string IlkKalkisNoktaID { get; set; }
		
		public string IlkKalkisNokta { get; set; }
		
		public string KalkisNoktaID { get; set; }
		
		public string KalkisNokta { get; set; }
	
		public string VarisNoktaID { get; set; }
		
		public string VarisNokta { get; set; }
		
		public string SonVarisNoktaID { get; set; }
		
		public string SonVarisNokta { get; set; }
		
		public string OtobusTipi { get; set; }
		
		public string OtobusKoltukYerlesimTipi { get; set; }
		
		public string OTipAciklamasi { get; set; }
		
		public string OtobusTelefonu { get; set; }
	
		public string OtobusPlaka { get; set; }
		
		public DateTime SeyahatSuresi { get; set; }
		public string StrSeyahatSuresi { get; set; }

		public string SeyahatSuresiGosterimTipi { get; set; }
		
		public string YaklasikSeyahatSuresi { get; set; }
		
		public string BiletFiyati1 { get; set; }
		
		public string BiletFiyatiInternet { get; set; }
		
		public string Sinif_Farki { get; set; }
		
		public string MaxRzvZamani { get; set; }
		
		public string SeferTipi { get; set; }
		
		public string SeferTipiAciklamasi { get; set; }
		
		public string HatSeferNo { get; set; }
	
		public string O_Tip_Sinif { get; set; }

		public string SeferTakipNo { get; set; }
	
		public string ToplamSatisAdedi { get; set; }

		public string DolulukKuraliVar { get; set; }
		
		public string OTipOzellik { get; set; }
	
		public string NormalBiletFiyati { get; set; }
	
		public string DoluSeferMi { get; set; }

		public string Tesisler { get; set; }

		public string SeferBosKoltukSayisi { get; set; }

		public string KalkisTerminalAdi { get; set; }

		public string KalkisTerminalAdiSaatleri { get; set; }
		
		public string MaximumRezerveTarihiSaati { get; set; }
	
		public string Guzergah { get; set; }
	
		public string KKZorunluMu { get; set; }
		
		public string BiletIptalAktifMi { get; set; }
		
		public string AcikParaKullanimAktifMi { get; set; }
		
		public string SefereKadarIptalEdilebilmeSuresiDakika { get; set; }

		public string FirmaSeferAciklamasi { get; set; }
		public string SatisYonlendirilecekMi { get; set; }

        public List<TipOzellikViewModel> Ozellikler { get; set; }
    }
	public class TipOzellikViewModel
    {
		
		public string O_Tip_Ozellik { get; set; }

		public string O_Tip_Ozellik_Aciklama { get; set; }

		public string O_Tip_Ozellik_Detay { get; set; }
		public string O_Tip_Ozellik_Icon { get; set; }
	}
}
