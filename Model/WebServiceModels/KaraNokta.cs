﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Model.WebServiceModels
{
	[XmlRoot(ElementName = "KaraNokta")]
	public class KaraNokta
	{
		[XmlElement(ElementName = "ID")]
		public int ID { get; set; }
		[XmlElement(ElementName = "SeyahatSehirID")]
		public int SeyahatSehirID { get; set; }
		[XmlElement(ElementName = "Bolge")]
		public string Bolge { get; set; }
		[XmlElement(ElementName = "Ad")]
		public string Ad { get; set; }
		[XmlElement(ElementName = "Aciklama")]
		public string Aciklama { get; set; }
		[XmlElement(ElementName = "MerkezMi")]
		public int MerkezMi { get; set; }
		[XmlElement(ElementName = "BagliOlduguNoktaID")]
		public int BagliOlduguNoktaID { get; set; }
	}

	[XmlRoot(ElementName = "KaraNoktalar")]
	public class KaraNoktalar
	{
		[XmlElement(ElementName = "KaraNokta")]
		public List<KaraNokta> KaraNokta { get; set; }
	}
}
