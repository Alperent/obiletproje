using Entity;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace DataAccess
{
    public partial class OBiletDbContext : DbContext
    {
        public OBiletDbContext()
            : base("OBiletDbContext")
        {
            Database.SetInitializer(new DbInitializer());
        }

        public virtual DbSet<Bilet> Bilet { get; set; }
        public virtual DbSet<Sefer> Sefer { get; set; }
        public virtual DbSet<Yolcu> Yolcu { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bilet>()
                .Property(e => e.Pnr)
                .IsUnicode(false);

            modelBuilder.Entity<Bilet>()
                .Property(e => e.EPosta)
                .IsUnicode(false);

            modelBuilder.Entity<Bilet>()
                .Property(e => e.Telefon)
                .IsUnicode(false);

            modelBuilder.Entity<Bilet>()
                .HasMany(e => e.Yolcu)
                .WithRequired(e => e.Bilet)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.SeferNo)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.FirmaAdi)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.Guzergah)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.KalkisNoktasi)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.VarisNoktasi)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.Sure)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.KoltukTipi)
                .IsUnicode(false);

            modelBuilder.Entity<Sefer>()
                .Property(e => e.SeferTipi)
                .IsUnicode(false);



            modelBuilder.Entity<Sefer>()
                .HasMany(e => e.Bilet)
                .WithRequired(e => e.Sefer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Yolcu>()
                .Property(e => e.AdSoyad)
                .IsUnicode(false);

            modelBuilder.Entity<Yolcu>()
                .Property(e => e.Cinsiyet)
                .IsUnicode(false);

            modelBuilder.Entity<Yolcu>()
                .Property(e => e.Durum)
                .IsUnicode(false);
        }
    }
}
