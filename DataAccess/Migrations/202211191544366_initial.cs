﻿namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bilet",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Pnr = c.String(nullable: false, maxLength: 50, unicode: false),
                        SeferId = c.Int(nullable: false),
                        EPosta = c.String(nullable: false, maxLength: 100, unicode: false),
                        Telefon = c.String(nullable: false, maxLength: 50, unicode: false),
                        ToplamUcret = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sefer", t => t.SeferId)
                .Index(t => t.SeferId);
            
            CreateTable(
                "dbo.Sefer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SeferNo = c.String(nullable: false, maxLength: 50, unicode: false),
                        FirmaAdi = c.String(nullable: false, maxLength: 50, unicode: false),
                        Guzergah = c.String(nullable: false, maxLength: 300, unicode: false),
                        KalkisNoktasi = c.String(nullable: false, maxLength: 50, unicode: false),
                        VarisNoktasi = c.String(nullable: false, maxLength: 50, unicode: false),
                        KalkisTarihi = c.DateTime(nullable: false),
                        Sure = c.String(nullable: false, maxLength: 50, unicode: false),
                        KoltukTipi = c.String(nullable: false, maxLength: 50, unicode: false),
                        SeferTipi = c.String(nullable: false, maxLength: 50, unicode: false),
                        Peron = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Yolcu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BiletId = c.Int(nullable: false),
                        AdSoyad = c.String(nullable: false, maxLength: 50, unicode: false),
                        Durum = c.String(nullable: false, maxLength: 50, unicode: false),
                        KoltukNo = c.Int(nullable: false),
                        Cinsiyet = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bilet", t => t.BiletId)
                .Index(t => t.BiletId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Yolcu", "BiletId", "dbo.Bilet");
            DropForeignKey("dbo.Bilet", "SeferId", "dbo.Sefer");
            DropIndex("dbo.Yolcu", new[] { "BiletId" });
            DropIndex("dbo.Bilet", new[] { "SeferId" });
            DropTable("dbo.Yolcu");
            DropTable("dbo.Sefer");
            DropTable("dbo.Bilet");
        }
    }
}
