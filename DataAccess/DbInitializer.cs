﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DbInitializer : CreateDatabaseIfNotExists<OBiletDbContext>
    {
        protected override void Seed(OBiletDbContext context)
        {
           
            var sefer = new Sefer 
            {
                FirmaAdi = "Metro Turizm",
                Guzergah = "KAYSERİ > ANKARA (AŞTİ)",
                KalkisNoktasi = "KAYSERİ",
                KalkisTarihi = new DateTime(2022, 11, 25, 14, 30, 0),
                KoltukTipi = "2+1",
                Peron = 9,
                SeferNo = "500",
                SeferTipi = "MOLALI",
                Sure = "5 Saat",
                VarisNoktasi = "ANKARA (AŞTİ)"
            };
            List<Yolcu> yolcular = new List<Yolcu>
            {
                new Yolcu{
                     AdSoyad="Alperen Topcuoğlu",
                Cinsiyet="Erkek",
                Durum="Rezervasyon",
                KoltukNo=11,
                },
                new Yolcu{
                     AdSoyad = "Mehmet Topcuoğlu",
                Cinsiyet = "Erkek",
                Durum = "Rezervasyon",
                KoltukNo = 12,
                },
            };
            var bilet =new Bilet() { EPosta = "alperen@gmail.com", Telefon = "0555 273 35 70",Pnr="ABC100",ToplamUcret=550,Sefer=sefer,Yolcu=yolcular};

            context.Bilet.Add(bilet);

            base.Seed(context);
        }
    }
}
