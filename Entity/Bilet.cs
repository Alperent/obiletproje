namespace Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Bilet")]
    public partial class Bilet
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Bilet()
        {
            Yolcu = new HashSet<Yolcu>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Pnr { get; set; }

        public int SeferId { get; set; }

        [Required]
        [StringLength(100)]
        public string EPosta { get; set; }

        [Required]
        [StringLength(50)]
        public string Telefon { get; set; }

        public double ToplamUcret { get; set; }

        public virtual Sefer Sefer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Yolcu> Yolcu { get; set; }
    }
}
