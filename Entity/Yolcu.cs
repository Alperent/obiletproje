namespace Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Yolcu")]
    public partial class Yolcu
    {
        public int Id { get; set; }

        public int BiletId { get; set; }

        [Required]
        [StringLength(50)]
        public string AdSoyad { get; set; }

        [Required]
        [StringLength(50)]
        public string Durum { get; set; }

        public int KoltukNo { get; set; }
        [Required]
        [StringLength(50)]
        public string Cinsiyet { get; set; }

        public virtual Bilet Bilet { get; set; }
    }
}
