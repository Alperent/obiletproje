namespace Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Sefer")]
    public partial class Sefer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sefer()
        {
            Bilet = new HashSet<Bilet>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string SeferNo { get; set; }

        [Required]
        [StringLength(50)]
        public string FirmaAdi { get; set; }

        [Required]
        [StringLength(300)]
        public string Guzergah { get; set; }

        [Required]
        [StringLength(50)]
        public string KalkisNoktasi { get; set; }

        [Required]
        [StringLength(50)]
        public string VarisNoktasi { get; set; }

        public DateTime KalkisTarihi { get; set; }

        [Required]
        [StringLength(50)]
        public string Sure { get; set; }

        [Required]
        [StringLength(50)]
        public string KoltukTipi { get; set; }

        [Required]
        [StringLength(50)]
        public string SeferTipi { get; set; }

   

        public int Peron { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bilet> Bilet { get; set; }
    }
}
